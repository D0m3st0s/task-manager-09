package ru.shumov.tm.service;

import java.security.NoSuchAlgorithmException;

public interface Md5Service {

    String md5(String string) throws NoSuchAlgorithmException;
}
