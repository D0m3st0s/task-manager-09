package ru.shumov.tm.service;

import ru.shumov.tm.entity.User;

import java.util.Collection;

public interface UserService {

    User getOne(String login);

    void create(User user);

    void update(User user);
}
