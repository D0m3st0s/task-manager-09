package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.repository.UserRepository;

import java.util.Collection;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Collection<User> getList() {
        return userRepository.findAll();
    }

    public User getOne(@NotNull String login) {
        return userRepository.findOne(login);
    }

    public void create(@Nullable User user) {
        try {
            if (user.getLogin() == null || user.getLogin().isEmpty()) {
                throw new NullPointerException();
            }
        }
        catch (NullPointerException exception){
            exception.printStackTrace();
        }
        userRepository.persist(user);
    }

    public void update(@NotNull User user) {
        userRepository.merge(user);
    }
}
