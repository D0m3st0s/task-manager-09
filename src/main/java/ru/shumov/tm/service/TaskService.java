package ru.shumov.tm.service;

import ru.shumov.tm.entity.Task;

import java.util.Collection;

public interface TaskService {

    void create(Task task);

    void update(Task task);

    void clear();

    void remove(String taskId);

    Collection<Task> getList();

    Task getOne(String id);

    boolean checkKey(String id);
}