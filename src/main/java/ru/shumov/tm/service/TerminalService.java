package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.entity.User;

public interface TerminalService {

    String scanner();

    void outPutString(@NotNull String string);

    void outPutProject(@NotNull Project project);

    void outPutTask(@NotNull Task task);

    void outPutUser(@NotNull User user);
}
