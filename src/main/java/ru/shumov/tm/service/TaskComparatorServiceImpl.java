package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Task;

import java.util.Comparator;

public class TaskComparatorServiceImpl implements TaskComparatorService, Comparator<Task> {
    private String method;

    public TaskComparatorServiceImpl(String method) {
        this.method = method;
    }

    public int compare(@NotNull final Task ts1, @NotNull final Task ts2) {
        switch (method) {
            case "by creating date":
                return ts1.getCreatingDate().compareTo(ts2.getCreatingDate());
            case "by start date":
                return ts1.getStartDate().compareTo(ts2.getStartDate());
            case "by end date":
                return ts1.getEndDate().compareTo(ts2.getEndDate());
            case "by status":
                return Integer.compare(ts1.getStatus().getNumber(), ts2.getStatus().getNumber());
            default:
                return 0;
        }
    }
}
