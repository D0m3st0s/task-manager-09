package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.ProjectRepository;

import java.util.Collection;

public class ProjectServiceImpl implements ProjectService {
    private ProjectRepository projectRepository;
    private TaskService taskService;
    private TerminalServiceImpl data;

    public ProjectServiceImpl(ProjectRepository projectRepository, TerminalServiceImpl data, TaskService taskService) {
        this.projectRepository = projectRepository;
        this.data = data;
        this.taskService = taskService;
    }

    public void create(@NotNull Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.persist(project.getId(), project);
        }
        catch (IncorrectInputException incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public void clear() {
        projectRepository.removeAll();
        taskService.clear();
    }

    public void remove(@Nullable String projectId) {
        try {
            if (projectId == null || projectId.isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project id");
            }
            projectRepository.remove(projectId);
        }
        catch (IncorrectInputException incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public Collection<Project> getList() {
        Collection<Project> values = projectRepository.findAll();
        return values;
    }

    public Project getOne(@NotNull String id) {
        return projectRepository.findOne(id);
    }

    public void update(@Nullable Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.merge(project);
        }
        catch (IncorrectInputException incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public boolean checkKey(String id) {
        if(projectRepository.findOne(id) != null) {
            return projectRepository.findOne(id).getId().equals(id);
        }
        return false;
    }
}
