package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Task;

public interface TaskComparatorService {
    int compare(@NotNull final Task ts1, @NotNull final Task ts2);
}
