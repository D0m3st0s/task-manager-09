package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Project;

public interface ProjectComparatorService {
    int compare(@NotNull final Project pr1, @NotNull final Project pr2);
}
