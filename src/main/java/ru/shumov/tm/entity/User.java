package ru.shumov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.enums.Role;

import java.util.UUID;

@Getter
@Setter
public class User {

    private Role role;
    private String password;
    private String login;
    private String id = UUID.randomUUID().toString();

    @Override
    public String toString() {
        return "User{" +
                "role=" + role.getDisplayName() +
                ", login='" + login + '\'' +
                '}';
    }
}

