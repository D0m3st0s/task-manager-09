package ru.shumov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.enums.Status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
public class Project {
    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private String id;
    private String name;
    private String description;
    private Date creatingDate;
    private Date startDate;
    private Date endDate;
    private String UserId;
    private Status status = Status.PLANNED;

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateOfCreating=" + dateFormat.format(creatingDate) +
                ", startDate=" + dateFormat.format(startDate) +
                ", endDate=" + dateFormat.format(endDate) +
                ", status=" + status +
                '}';
    }
}
