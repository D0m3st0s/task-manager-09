package ru.shumov.tm;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.shumov.tm.command.AbstractCommand;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.*;
import ru.shumov.tm.service.*;
import ru.shumov.tm.service.TerminalServiceImpl;
import ru.shumov.tm.service.Md5ServiceImpl;

import java.security.NoSuchAlgorithmException;
import java.util.Set;

import static ru.shumov.tm.Constants.*;

public class Bootstrap implements ServiceLocator {

    @Getter
    private TerminalServiceImpl terminalService = new TerminalServiceImpl();
    private TaskRepository taskRepository = new TaskRepositoryImpl();
    @Getter
    private TaskService taskService = new TaskServiceImpl(taskRepository, terminalService);
    private ProjectRepository projectRepository = new ProjectRepositoryImpl();
    @Getter
    private ProjectService projectService = new ProjectServiceImpl(projectRepository, terminalService, taskService);
    private UserRepository userRepository = new UserRepositoryImpl();
    @Getter
    private UserService userService = new UserServiceImpl(userRepository);
    @Getter
    private CommandsService commandsService = new CommandsServiceImpl(this);
    @Getter
    private Md5ServiceImpl md5Service = new Md5ServiceImpl();
    @Getter
    @Setter
    private User user = null;
    private boolean work = true;

    public void setWork(boolean work) {
        this.work = work;
    }

    public void init() {
        try {
            initCommand();
            userRepository.usersCreate();
            start();
        } catch (Exception exception) {
            terminalService.outPutString("При работе приложения возникла ошибка.");
            exception.printStackTrace();
        }
    }

    public void start() {
        terminalService.outPutString(WELCOME);
        terminalService.outPutString(PROJECT_ID);
        while (work) {
            try {
                String commandName = terminalService.scanner();
                if (commandsService.checkKey(commandName)) {
                    commandsService.commandExecute(commandName);
                } else {
                    terminalService.outPutString(COMMAND_DOES_NOT_EXIST);
                }
            } catch (Exception exception) {
                terminalService.outPutString("При работе приложения возникла ошибка.");
                exception.printStackTrace();
            }
        }
    }

    public void initCommand() {
        AbstractCommand command;

        final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.shumov.tm.command").getSubTypesOf(AbstractCommand.class);
        try {
            for (@NotNull final Class clazz : classes) {
                if (AbstractCommand.class.isAssignableFrom(clazz)) {
                    command = (AbstractCommand) clazz.newInstance();
                    regCommand(command);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void regCommand(AbstractCommand command) {
        @NotNull final String commandName = command.getName();
        @NotNull final String commandDescription = command.getDescription();
        try {
            if (commandName == null || commandName.isEmpty()) {
                throw new IncorrectInputException("Invalid Command Name: " + commandName);
            }
            if (commandDescription == null || commandDescription.isEmpty()) {
                throw new IncorrectInputException("Invalid Command Description" + commandDescription);
            }
            commandsService.commandAdd(command);
        } catch (IncorrectInputException e) {
            terminalService.outPutString(e.getMassage());
        }
    }
}
