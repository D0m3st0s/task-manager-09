package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.enums.Role;

import java.security.NoSuchAlgorithmException;

@Getter
public class UserShowCommand extends AbstractCommand {
    private final Role role = Role.USER;
    private final String name = "show";
    private final String description = "show: Вывод информации о пользователе.";

    public void execute() throws NoSuchAlgorithmException {
        @Nullable var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.USER_DID_NOT_AUTHORIZED);
            return;
        }
        bootstrap.getTerminalService().outPutUser(user);
    }

    public UserShowCommand() {
    }
}
