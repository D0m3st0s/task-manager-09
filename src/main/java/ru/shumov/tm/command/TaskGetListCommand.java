package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.enums.Role;

import java.util.Collection;

public class TaskGetListCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "task list";
    @Getter
    private final String description = "task list: Вывод всех задач.";

    @Override
    public void execute() {
        var counter = 0;
        @Nullable final var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            @Nullable Collection<Task> values = bootstrap.getTaskService().getList();
            if (values.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.TASKS_DO_NOT_EXIST);
                return;
            }
            for (Task task : values) {
                if (task.getUserId().equals(bootstrap.getUser().getId())) {
                    counter++;
                }
            }
            if (counter == 0) {
                bootstrap.getTerminalService().outPutString(Constants.TASKS_DO_NOT_EXIST);
                return;
            }
            for (Task task : values) {
                if (task.getUserId().equals(bootstrap.getUser().getId())) {
                    bootstrap.getTerminalService().outPutTask(task);
                }
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskGetListCommand() {
    }
}
