package ru.shumov.tm.command;

import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.ServiceLocator;
import ru.shumov.tm.enums.Role;

import java.security.NoSuchAlgorithmException;

public abstract class AbstractCommand {
    @Getter
    private String name;
    @Getter
    private String description;
    @Setter
    protected ServiceLocator bootstrap;
    @Getter
    private Role role;

    public abstract void execute() throws NoSuchAlgorithmException;
}
