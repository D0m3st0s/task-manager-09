package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.enums.Role;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class ProjectCreateCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "project create";
    @Getter
    private final String description = "project create: Создание нового проекта.";

    @Override
    public void execute() {
        @Nullable var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            try {
                var format = new SimpleDateFormat();
                var dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                var date = LocalDate.now();
                var creatingDate = dateTimeFormatter.format(date);
                format.applyPattern("dd.MM.yyyy");
                bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_NAME);
                @NotNull final var name = bootstrap.getTerminalService().scanner();
                Project project = new Project();
                @NotNull final var userId = bootstrap.getUser().getId();
                @NotNull final var id = UUID.randomUUID().toString();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_START_DATE_OF_PROJECT);
                @NotNull final var startDate = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DEADLINE_OF_PROJECT);
                @NotNull final var endDate = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DESCRIPTION_OF_PROJECT);
                @NotNull final var description = bootstrap.getTerminalService().scanner();

                project.setDescription(description);
                project.setName(name);
                project.setCreatingDate(format.parse(creatingDate));
                project.setStartDate(format.parse(startDate));
                project.setEndDate(format.parse(endDate));
                project.setId(id);
                project.setUserId(userId);
                bootstrap.getProjectService().create(project);

            } catch (ParseException parseException) {
                bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public ProjectCreateCommand() {
    }
}
