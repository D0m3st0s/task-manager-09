package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.enums.Role;

public class TaskGetOneCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "get task";
    @Getter
    private final String description = "get task: вывод конкретной задачи.";

    @Override
    public void execute() {
        @Nullable var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_ID_FOR_SHOWING_TASKS);
            @NotNull final var taskId = bootstrap.getTerminalService().scanner();
            @NotNull final var task = bootstrap.getTaskService().getOne(taskId);
            if (task.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService().outPutTask(task);
            } else {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskGetOneCommand() {
    }
}
