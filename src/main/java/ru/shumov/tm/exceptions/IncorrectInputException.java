package ru.shumov.tm.exceptions;

import lombok.Getter;

public class IncorrectInputException extends Exception{
    @Getter
    private String massage;

    public IncorrectInputException(String s) {
        this.massage = s;
    }
}
