package ru.shumov.tm.enums;

import lombok.Getter;

public enum Status {

    PLANNED("Запланированно", 2),
    IN_PROCESS("В процессе", 1),
    DONE("Готово", 0);

    @Getter
    private String displayName;
    @Getter
    private int number;

    Status(String displayName, int number) {
        this.displayName = displayName;
        this.number = number;
    }
}
