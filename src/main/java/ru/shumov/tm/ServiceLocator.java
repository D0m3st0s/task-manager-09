package ru.shumov.tm;

import ru.shumov.tm.entity.User;
import ru.shumov.tm.service.*;

public interface ServiceLocator {

    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    User getUser();

    Md5ServiceImpl getMd5Service();

    TerminalServiceImpl getTerminalService();

    void setUser(User user);

    CommandsService getCommandsService();

    void setWork(boolean work);
}
