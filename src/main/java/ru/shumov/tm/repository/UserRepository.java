package ru.shumov.tm.repository;

import ru.shumov.tm.entity.User;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public interface UserRepository {

    void persist(User user);

    void merge(User user);

    User findOne(String login);

    Collection<User> findAll();

    void usersCreate() throws NoSuchAlgorithmException;
}
