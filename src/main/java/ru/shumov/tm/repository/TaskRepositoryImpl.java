package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Task;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TaskRepositoryImpl implements TaskRepository {

    private Map<String, Task> tasks = new HashMap<>();

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findOne(@NotNull String id) {
        return tasks.get(id);
    }

    public void persist(@NotNull Task task) {
        if(!tasks.containsKey(task.getId())){
            tasks.put(task.getId(), task);
        }
    }

    public void merge(@NotNull Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(@NotNull String id) {
        tasks.remove(id);
    }

    public void removeAll() {
        tasks.clear();
    }
}
