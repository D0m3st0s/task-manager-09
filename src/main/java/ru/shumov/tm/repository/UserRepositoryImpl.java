package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.service.Md5ServiceImpl;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserRepositoryImpl implements UserRepository {

    private Md5ServiceImpl utils = new Md5ServiceImpl();

    private Map<String, User> users = new HashMap<>();

    public void persist(@NotNull User user) {
        if(!users.containsKey(user.getLogin())) {
            users.put(user.getLogin(), user);
        }
    }

    public void merge(@NotNull User user) {
        users.put(user.getId(), user);
    }

    public Collection<User> findAll() {
        return users.values();
    }

    public User findOne(@NotNull String login) {
        return users.get(login);
    }

    public void usersCreate() throws NoSuchAlgorithmException {
        User admin = new User();
        admin.setLogin("admin");
        admin.setRole(Role.ADMINISTRATOR);
        String adminPassword = "admin";
       adminPassword = utils.md5(adminPassword);
        admin.setPassword(adminPassword);
        users.put(admin.getLogin(), admin);
        User user = new User();
        user.setRole(Role.USER);
        user.setLogin("user");
        String userPassword = "user";
        userPassword = utils.md5(userPassword);
        user.setPassword(userPassword);
        users.put(user.getLogin(), user);
    }
}
