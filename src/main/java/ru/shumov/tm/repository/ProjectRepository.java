package ru.shumov.tm.repository;

import ru.shumov.tm.entity.Project;

import java.util.Collection;

public interface ProjectRepository {

    Collection<Project> findAll();

    Project findOne(String id);

    void persist(String id, Project project);

    void merge(Project project);

    void remove(String id);

    void removeAll();
}
